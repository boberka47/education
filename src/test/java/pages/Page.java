package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import javax.management.StringValueExp;

public class Page {

    public WebDriver driver;

    @FindBy (xpath = "//html/body/section/div[3]/div/div/form/div/div[2]/div[1]/button")
        public static WebElement ReceiveMoney;


    @FindBy(name = "username")
    public static WebElement fieldName;

    @FindBy(name = "password")
    public static WebElement fieldPassword;

    @FindBy(xpath = "//*[@id=\"login\"]/button/i")
    public static WebElement enterLogin;

    public static void clickButton(WebDriver driver, WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }


    @FindBy(xpath = "//*[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]")
    public static WebElement successLogin;

    @FindBy(xpath = "//*[contains(text(),'Your username is invalid!')]")
    public static WebElement wrongLoginName;

    @FindBy(xpath = "//*[contains(text(),'Your password is invalid!')]")
    public static WebElement wrongLoginPassword;

    public Page(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public static void openLoginPage(WebDriver driver) {
        driver.get("http://the-internet.herokuapp.com/login");
    }

    public static void openAnyPage(WebDriver driver, String url) {
        driver.get(url);
    }

    public static void maximizeBrowser(WebDriver driver) {
        driver.manage().window().maximize();
    }

    public static void checkElementDisplayed(WebElement element) {
        Assert.assertTrue(element.isDisplayed());
    }

    public static void enterField(WebElement element, String value) {
        element.sendKeys(value);
    }

}
