import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.Base;
import pages.Page;

import static helpers.Data.*;
import static pages.Base.makeScreenOnTestFail;
import static pages.Page.*;
import static pages.Page.checkElementDisplayed;

public class example_PO_Data {

    public WebDriver driver;
    private Page newPage;
    public Base Base;

    @BeforeMethod
    public void setupClass() {
        //WebDriverManager.chromedriver().setup();
        Base = new Base();
        driver = Base.initialize_driver();
        newPage = new Page(driver);
    }

    @Test(description= "First task", priority = 0)
    public void testRightEnter() {

        maximizeBrowser(driver);

        openAnyPage(driver, url);

        //enterField(fieldName, rightLogin);

        //enterField(fieldPassword, rightPassword);

        //enterLogin.click();

        //clickButton(driver,enterLogin);

        //checkElementDisplayed(successLogin);

        System.out.println("First task - OK");

    }

    @Test(description= "Second and Third task", priority = 1)
    public void testErrorEnter() {
        maximizeBrowser(driver);

        openAnyPage(driver, url);

        //enterField(fieldName, wrongLogin);

        //enterField(fieldPassword, rightPassword);

        //enterLogin.click();

        //clickButton(driver,enterLogin);

        //checkElementDisplayed(wrongLoginName);

        //System.out.println("Second task - OK");

        //enterField(fieldName, rightLogin);

        //enterField(fieldPassword, wrongPassword);

        //enterLogin.click();

        //clickButton(driver,enterLogin);

        //checkElementDisplayed(wrongLoginPassword);

        System.out.println("Third task - OK");

    }

    @AfterMethod
    public void teardown(ITestResult result) {
        makeScreenOnTestFail(result);
        if (driver!=null) {
            driver.quit();
        }
    }

}
