import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class example {

    public WebDriver driver;

    @BeforeMethod
    public void setupClass() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @Test(description= "First task", priority = 0)
    public void testRightEnter() {
        driver.get("http://the-internet.herokuapp.com/login");
        System.out.println("Page title is: " + driver.getTitle());
        WebElement element1 = driver.findElement(By.name("username"));
        element1.sendKeys("tomsmith");
        WebElement element2 = driver.findElement(By.name("password"));
        element2.sendKeys("SuperSecretPassword!");
        driver.findElement(By.xpath("//*[@id=\"login\"]/button/i")).click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Welcome to the Secure Area. When you are done click logout below.')]")).isDisplayed());
        driver.findElement(By.xpath("//*[@id=\"content\"]/div/a/i")).click();
        System.out.println("First task - OK");
    }

    @Test(description= "Second and Third task", priority = 1)
    public void testErrorEnter() {
        driver.manage().window().maximize();
        driver.get("http://the-internet.herokuapp.com/login");
        WebElement element1 = driver.findElement(By.name("username"));
        element1.sendKeys("tomsmit");
        WebElement element2 = driver.findElement(By.name("password"));
        element2.sendKeys("SuperSecretPassword!");
        driver.findElement(By.xpath("//*[@id=\"login\"]/button/i")).click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Your username is invalid!')]")).isDisplayed());
        System.out.println("Second task - OK");

        WebElement element3 = driver.findElement(By.name("username"));
        element3.sendKeys("tomsmith");
        WebElement element4 = driver.findElement(By.name("password"));
        element4.sendKeys("SuperSecretPassword");
        driver.findElement(By.xpath("//*[@id=\"login\"]/button/i")).click();

        Assert.assertTrue(driver.findElement(By.xpath("//*[contains(text(),'Your password is invalid!')]")).isDisplayed());
        System.out.println("Third task - OK");

    }


    @AfterMethod
    public void teardown() {
        if (driver!=null) {
            driver.quit();
        }
    }
}
